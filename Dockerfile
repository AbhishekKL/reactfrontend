FROM node:12.3.1-alpine

# Change directory so that our commands run inside this new directory
WORKDIR /app

# Install serve globally
RUN npm install -g --verbose serve

# Copy static files from build image
COPY build ./

# Expose the port the app runs in
EXPOSE 3010

# Serve the application
CMD ["serve","-s",".","-l","3000"]
