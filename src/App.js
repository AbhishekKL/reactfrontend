import React, { Component } from 'react';

//import ChartComponent from './Components/ChartComponent'
import Selection from './Components/selection'
import LoginComponent from './Components/LoginComponent'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Cookies from 'universal-cookie';
const cookies = new Cookies();


class App extends Component {

  constructor() {
    super();
    this.responseFromLogin = this.responseFromLogin.bind(this);
    this.logout = this.logout.bind(this);
    this.state = {
      username: '',
      showLoginComponent: true,
      showselectionComponent: false,
    };

  }

  componentDidMount() {
    if(cookies.get('username')){
      this.setState({
        username : cookies.get('username'),
        showLoginComponent:false,
        showselectionComponent:true
      })
      document.getElementById('main').style.display = 'block';
    }
    
  }

  responseFromLogin(responsefromlogin){
    this.setState({
      username : responsefromlogin.username,
      showLoginComponent:false,
      showselectionComponent:true
    })
    document.getElementById('main').style.display = 'block';
  }

logout(){
  cookies.remove('username');
  this.setState({
    username:'',
    showLoginComponent:true,
    showselectionComponent:false
  })
  document.getElementById('main').style.display = 'none';
}

  render() {
    return (
      <div className="app">

      { this.state.showLoginComponent ? <LoginComponent callbackFromApp={this.responseFromLogin} /> : null}

      <div id="main" style={{display: 'none'}}>
        <nav className="navbar navbar-inverse fixed-top" style={{'backgroundColor':'#e41937','marginBottom': '15px'}}>
          <div className="container-fluid">
            <div className="navbar-header">
              <p className="navbar-brand" style={{textAlign: 'center','color':'white','marginBottom': '0px','marginRight': '0px'}}>TeachMahindra BookMySeat DashBoard</p>
            </div>
            <button className="btn nav navbar-nav navbar-right" style={{'color':'white'}} onClick={this.logout}> LogOut </button>
          </div>
        </nav>

        <div style ={{marginTop:'60px'}}>
                    <div>
                        <h6>
                            <p style={{textAlign: 'center', color:" #8E8E8E" , fontWeight: 'bold'}}>
                            Welcome to BookMySeat DashBoard,<b> {this.state.username}.</b>
                            </p>
                        </h6>
                    </div>
        </div>
      </div>
        {this.state.showselectionComponent ? <Selection /> : null }
        


      </div>
    )
  }

}

export default App;
