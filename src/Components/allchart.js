import React, { Component } from 'react';
import "react-datepicker/dist/react-datepicker.css";
import Seatsummarychart from './charts/seatsummarychart'
import Seatsummarybyodc from './charts/seatsummarybyodc'
import Hotseatbycity from './charts/hotseatbycity';
import Hotseatbyseatstatus from './charts/hotseatbyseatstatus'
import Hotseatbyleavetype from './charts/hotseatbyleavetype'
import Hotseatbooked from './charts/hotseatbooked'
import Topfiveodc from './charts/topfiveodc'
import Hotseatsbookedcity from './charts/hotseatsbookedcity'
import Hotseatbyodctype from './charts/hotseatbyodctype'

export default class Allchart extends Component {
    render(){
        //sel StartDate EndDate Location
        let sel = {
            sdate:this.props.infoToAllchart.statefromselection.fstartDate,
            edate:this.props.infoToAllchart.statefromselection.fendDate,
            city:this.props.infoToAllchart.statefromselection.selectedcity
        }
        return(
            <div className="Allchart">
             <div className="row">
                    <div className="col-md-6">
                        <Seatsummarychart info = {{sel}}/>
                    </div>
                    <div className="col-md-6">
                        <Seatsummarybyodc info = {{sel}}/>
                    </div>
            </div>
            <div className="row">
                    <div className="col-md-6">
                        <Hotseatbyleavetype info = {{sel}}/>
                    </div>
                    <div className="col-md-6">
                        <Hotseatbyodctype info ={{sel}}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <Hotseatbyseatstatus info = {{sel}}/>
                    </div>
                    <div className="col-md-6">
                    <Hotseatbycity info = {{sel}}/>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                    <Hotseatbooked info ={{sel}}/>
                    </div>
                    <div className="col-md-6">
                    <Hotseatsbookedcity info = {{sel}}/> 
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <Topfiveodc info ={{sel}}/>
                    </div>
                    
                </div>
                
                </div>
        )
    }
}