import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from "react-datepicker";
import moment from 'moment'
import "react-datepicker/dist/react-datepicker.css";
import Allchart from './allchart';
import endpoints from '../resources/config';


export default class BasicComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startDate: new Date(),
            endDate: new Date(),
            fstartDate: '',
            fendDate: '',
            cities: [],
            selectedcity: '',
            showChart: 0,
            minDate: moment(Date.now() - 7 * 24 * 3600 * 1000).toDate()
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleChange2 = this.handleChange2.bind(this);
        this.getCities = this.getCities.bind(this);
        this.listCities = this.listCities.bind(this);
        this.showallchart = this.showallchart.bind(this);

        
    }

    handleChange(date) {
        this.setState({
          startDate: date,
          endDate: date,
        })
      }

      handleChange2(date) {
        this.setState({
          endDate: date,
        })
      }

    getCities(e){
        this.setState({
            selectedcity: e.target.value,
        })
    }

    showallchart(e){
        e.preventDefault();
        this.setState({
            showChart : 0,
            fstartDate:moment(this.state.startDate).format('YYYY-MM-DD'),
            fendDate:moment(this.state.endDate).format('YYYY-MM-DD'),
        },()=>{
            this.setState({
                showChart : 1
            })
        })


    }
   
    

    listCities(){
        return this.state.cities.map(function(city, i){
            return(
                <option key={i} value={city}>{city}</option>
            );
        });
    }
    
    


    componentWillMount() {
        this.setState({
            fstartDate:moment(this.state.startDate).format('YYYY-MM-DD'),
            fendDate:moment(this.state.endDate).format('YYYY-MM-DD'),
        })
        axios.get(endpoints.backendHN+'/india/uniqueCities')
            .then(response => {
                this.setState({cities: response.data});
            }).catch(err => {
                console.log(err);
            });

    }

    


    render() {
        return (
            <div className="Selection container">
                <form>
                    <div className="row">
                        <div className="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <label><h6> From: </h6></label>
                            <DatePicker className="form-control" selected={this.state.startDate} minDate={this.state.minDate} maxDate={new Date()}  dateFormat="dd-MMM-yyyy" onChange={this.handleChange} required/>
                        </div>
                        <div className="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <label><h6> To: </h6></label>
                            <DatePicker className="form-control" selected={this.state.endDate} minDate={this.state.startDate} maxDate={new Date()}  dateFormat="dd-MMM-yyyy" onChange={this.handleChange2}  required/>
                        </div>
                        <div className="form-group col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <label><h6> City: </h6></label>
                            <select className="form-control" onChange={this.getCities}  required>
                                <option value="">Select City</option>
                                <option value="0">ALL INDIA</option>
                              {this.listCities()}
                            </select>
                        </div>
                    </div>
                    
                    <div className="form-row justify-content-center">
                        <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <button className="btn btn-primary btn-block" disabled={(this.state.startDate)&& (this.state.endDate)&&(this.state.selectedcity)? false : true} onClick={this.showallchart} >Generate The Graph</button>
                        </div>
                    </div>
                </form>
                { this.state.showChart ? <Allchart infoToAllchart = {{ statefromselection : this.state}}/> : null }
            </div>
        );
    }
}