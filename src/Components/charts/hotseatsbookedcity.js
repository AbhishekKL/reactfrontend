import React, { Component } from 'react';
import axios from 'axios';
import {Bar} from 'react-chartjs-2';
import endpoints from '../../resources/config';

export default class seatsummarychart extends Component{

    constructor(props){
        super(props);
        //state start
        this.state = {
            dataForSeatSummaryByOdc:'',
            city:props.info.sel.city,
            sdate:props.info.sel.sdate,
            edate:props.info.sel.edate            
        }
       
        //state end
        //function bind
        this.getSeatSummaryByOdc = this.getSeatSummaryByOdc.bind(this);
        //function bind end

        //variable start

        //variable end
    }

    componentDidMount(){
        this.getSeatSummaryByOdc();
        
        
    }

    //function define start

    getSeatSummaryByOdc(){
        axios.get(endpoints.backendHN+`/india/hotseat_booked_city_loc_wise/${this.state.sdate}/${this.state.edate}/${this.state.city}`)
        .then((response)=>{
            let dataseatsummaryodc =  {
                labels: response.data.city,
                datasets: [
                  {
                    label: 'Seats',
                    backgroundColor: [
                        '#ef6356',
                        '#ff9f40',
                        '#ffcd56',
                        '#4bc0c0',
                        '#36a2eb',
                        '#3f2356',
                        '#ff9540',
                        '#ffca56',
                        '#4bc2c0',
                        '#26a7eb'                               
                        ],
                    borderWidth: 1,
                    data: response.data.value
                  }
                ]
              };
              this.setState({dataForSeatSummaryByOdc:dataseatsummaryodc});

        }).catch((error)=>{
            console.log(error);
        })
    }
    //function define end


    

    render(){
        const chart = (this.state.dataForSeatSummaryByOdc ? (
            <Bar
                data={this.state.dataForSeatSummaryByOdc}
                width={500}
                height={250}
                options={{
                    plugins :{
                        datalabels: {
                            anchor:'end',
                            align : 'top',
                            color : 'black'
                        }
                    },
                    maintainAspectRatio: true,
                    responsive: false,
                    scales : {
                        yAxes : [{
                            ticks : {
                                beginAtZero : true
                            }   
                        }]
                    }
                }}
            />
        ): null);
        return(
            <div className="SeatSummartbyodc">
            <h3>Hot Seats Booked By City</h3>
            {chart}
            </div>
        )
    }
}