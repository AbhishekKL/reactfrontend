import React, { Component } from 'react';
import axios from 'axios';
import {Bar} from 'react-chartjs-2';
import endpoints from '../../resources/config';

export default class seatsummarychart extends Component{

    constructor(props){
        super(props);

        
        //state start
        this.state = {
            dataForSeatSummaryByOdc:'',
            city:props.info.sel.city,
            sdate:props.info.sel.sdate,
            edate:props.info.sel.edate            
        }
        //state start
        
        //state end
        //function bind
        this.getSeatSummaryByOdc = this.getSeatSummaryByOdc.bind(this);
        //function bind end

        //variable start

        //variable end
    }

    componentDidMount(){
        
        this.getSeatSummaryByOdc();
        
    }

    //function define start

    getSeatSummaryByOdc(){
        axios.get(endpoints.backendHN+`/india/hotseatavail_odctype/${this.state.sdate}/${this.state.edate}/${this.state.city}`)
        .then((response)=>{
            let dataseatsummaryodc = {
                labels: [
                    'Hot Seats',
                ],
                datasets: [
                  {
                    label: "ODC",
                    backgroundColor: "#03cece",
                    borderWidth: 1,
                    data: [response.data.odchotseat]
                  },
                  {
                    label: "NONODC",
                    backgroundColor: "#ff7f00",
                    borderWidth: 1,
                    data: [response.data.nonodchotseat]
                  }
                ]
              };
              this.setState({dataForSeatSummaryByOdc:dataseatsummaryodc});

        }).catch((error)=>{
            console.log(error);
        })
    }
    //function define end


    

    render(){
        const chart = (this.state.dataForSeatSummaryByOdc ? (
            <Bar
                data={this.state.dataForSeatSummaryByOdc}
                width={500}
                height={250}
                options={{
                    maintainAspectRatio: true,
                    responsive: false,
                    scales : {
                        yAxes : [{
                            ticks : {
                                beginAtZero : true
                            }   
                        }]
                    }
                }}
            />
        ): null);
        return(
            <div className="SeatSummartbyodc">
            <h3>Hot Seats By ODC Type</h3>
            {chart}
            </div>
        )
    }
}