import React, { Component } from 'react';
import axios from 'axios';
import {Bar} from 'react-chartjs-2';
import endpoints from '../../resources/config';

export default class seatsummarychart extends Component{

    constructor(props){
        super(props);
        //state start
        this.state = {
            dataForSeatSummaryByOdc:'',
            city:props.info.sel.city,
            sdate:props.info.sel.sdate,
            edate:props.info.sel.edate            
        }
       
        //state end
        //function bind
        this.getSeatSummaryByOdc = this.getSeatSummaryByOdc.bind(this);
        //function bind end

        //variable start

        //variable end
    }

    componentDidMount(){
        this.getSeatSummaryByOdc();
        
    }

    //function define start

    getSeatSummaryByOdc(){
        axios.get(endpoints.backendHN+`/india/seats_summary_by_odc/${this.state.city}`)
        .then((response)=>{
            let dataseatsummaryodc =  {
                labels: [
                    'Seats'
                ],
                datasets: [
                  {
                    label: "ODC Occupied",
                    backgroundColor: "#2193ef",
                    borderWidth: 1,
                    data: [response.data.odcoccupied]
                  },
                  {
                    label: "ODC Vacant",
                    backgroundColor: "#3bc7c7",
                    borderWidth: 1,
                    data: [response.data.odcvacant]
                  },
                  {
                    label: "NONODC Occupied",
                    backgroundColor: "#6593F5",
                    borderWidth: 1,
                    data: [response.data.nonodcoccupied]
                  },
                  {
                    label: "NONODC Vacant",
                    backgroundColor: "#ff9f40",
                    borderColor: "#ff9f40",
                    borderWidth: 1,
                    data: [response.data.nonodcvacant]
                  },

                ]
              };
              this.setState({dataForSeatSummaryByOdc:dataseatsummaryodc});

        }).catch((error)=>{
            console.log(error);
        })
    }
    //function define end


    

    render(){
        const chart = (this.state.dataForSeatSummaryByOdc ? (
            <Bar
                data={this.state.dataForSeatSummaryByOdc}
                width={500}
                height={250}
                options={{
                    maintainAspectRatio: true,
                    responsive: false,
                    scales : {
                        yAxes : [{
                            ticks : {
                                beginAtZero : true
                            }   
                        }]
                    }
                }}
            />
        ): null)
        return(
            <div className="SeatSummartbyodc">
            <h3>Seat Summary By Odc</h3>
            {chart}
            </div>
        )
    }
}