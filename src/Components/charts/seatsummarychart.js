import React, { Component } from 'react';
import axios from 'axios';
import {Pie} from 'react-chartjs-2';
import endpoints from '../../resources/config';


export default class seatsummarychart extends Component{

    constructor(props){
        super(props);
        //state start
        this.state = {
            dataforseatsummary:'',
            city:props.info.sel.city,
            sdate:props.info.sel.sdate,
            edate:props.info.sel.edate            
        }
        //state end
        //function bind
        this.getSeatSummary = this.getSeatSummary.bind(this);
        //function bind end

        //variable start
        
        //variable end
    }

    componentDidMount(){
        this.getSeatSummary();
    }

    //function define start
    getSeatSummary(){
        axios.get(endpoints.backendHN+`/india/seats_summary/${this.state.city}`)
        .then((response)=>{
            let dataseatsummary = {
                labels: ['Assocaiate Occupied','NONODC Vacant','ODC Vacant'],
                datasets: [
                  {
                    label: 'Seats',
                    backgroundColor: [
                        '#45a9ec',
                        '#ff9f40',
                        '#3bc7c7'
                        ],
                    borderWidth: 2,
                    data: response.data.value
                  }
                ]
              };
              this.setState({dataforseatsummary:dataseatsummary});

        }).catch((error)=>{
            
        })
        
    }


    
    //function define end


    

    render(){

        const charts = (this.state.dataforseatsummary) ?(
            <div><Pie 
                data={this.state.dataforseatsummary}
                width={500}
                height={250}
                options={{
                    maintainAspectRatio: true,
                    responsive: false
                }}
            />
            </div>
        ) : null;
        return(
            <div className="SeatSummartchart">
            <h3>Seat Summary</h3>
            {charts}

            </div>
        )
    }
}