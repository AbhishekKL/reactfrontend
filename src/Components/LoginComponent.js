import React, { Component } from 'react';
import axios from 'axios';
import Background from '../../src/background2.jpg'
import endpoints from '../resources/config';
// const Background = 'https://cdn.pixabay.com/photo/2017/08/30/01/05/milky-way-2695569_960_720.jpg';

import Cookies from 'universal-cookie';
const cookies = new Cookies();

export default class LoginComponent extends Component {
    constructor(props){
        super(props);
        this.onChangeEmailID = this.onChangeEmailID.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onLoginClick = this.onLoginClick.bind(this);
        this.onResetClick = this.onResetClick.bind(this);

        this.state = {
            emailid: '',
            password: '',
        };
    }

    onChangeEmailID(e){
        this.setState({emailid: e.target.value});
    }

    onChangePassword(e){
        this.setState({password: e.target.value});
    }

    onLoginClick(e) {
        e.preventDefault();
        axios.post(endpoints.backendHN+'/api/login', { "emailid": this.state.emailid, "pwd": this.state.password })
            .then(response => {
                if (response.data.id === 1) {
                    cookies.set('username', response.data.username);
                    this.props.callbackFromApp({
                        username: response.data.username,
                    });
                } else {
                    if(response.data.id === 5){
                    this.setState({ lanid: '', password: '' });
                    document.getElementById('errorMsgNew').style.display = 'block';
                    }
                    else{
                        this.setState({ lanid: '', password: '' });
                        document.getElementById('errorMsg').style.display = 'block';
                    }
                }
            }).catch(err => {
                console.log(err);
                //this.props.callbackFromApp('showError');
            });
    }

    onResetClick(e){
        this.setState({emailid: '', password: ''});
    }

    render() {
        return(
            <div className="Login" style = {{backgroundImage: `url(${Background})`}}>
            
            <div className="container-fluid">
                <div className="row">
                
                <div className="col-md-4 offset-md-7" style={{paddingTop:"15px", borderRadius:"6px", marginTop:"225px",backgroundColor:"white",boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 1), 0 6px 20px 0 rgba(0, 0, 0, 0.6)'}}>
                <form onSubmit={this.onLoginClick}>
                    <h6>
                           <p style={{fontSize: 'x-large'}}>Welcome to BookMySeat DashBoard</p>
                           <p style={{color: 'red', display: 'none'}} id="errorMsg">
                               The Email ID and/or Password entered is incorrect.  Please Try Again.
                           </p>
						 <p style={{ color: 'red', display: 'none' }} id="errorMsgNew">
                               You are already logged in by other device. To login from here logout from other device.
                          </p> 
                       </h6>
                    <div className="form-group">
                                   <input type="email" name="email" className="form-control" value={ this.state.emailid } onChange={ this.onChangeEmailID } placeholder="LanID@TechMahindra.com" required autoComplete="email" />
                    </div>
                               <div className="form-group">
                        <input type="password" name="current-password" className="form-control" value={ this.state.password } onChange={ this.onChangePassword } placeholder="Password" required autoComplete="current-password"/>                     </div>
                    <div className="form-group">
                        <input type="submit" className="btn btn-primary" value="Sign in" />
                               </div>
                            </form>
                </div>
                </div>
            </div>
            

                
            </div>
        );
    }
}
