const env = process.env.NODE_ENV || 'development';

//CONFIGURATION FOR DEV ENVIRONMENT
const development = {
    backendHN: 'http://localhost:8010',
};


//PLEASE FILL THE VALUES FOR PRODUCTION ENVIRONMENT
const production = {
    backendHN: 'https://bookmyseat.techmahindra.com',
};

//PLEASE FILL THE VALUES FOR TEST ENVIRONMENT
const test = {
    backendHN: '',
};

const endpoints = {
    development,
    production,
    test
};

module.exports = endpoints[env];
